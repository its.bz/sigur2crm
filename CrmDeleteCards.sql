declare @PeopleID bigint
declare @i int = 0

declare plist cursor for 
	select people_id from card_peoples where FULL_NAME LIKE '%';

open plist
fetch next from plist into @PeopleID 

while @@FETCH_STATUS = 0
begin
    delete from CARD_PEOPLE_PROPERTY_LINKS where PEOPLE_ID = @PeopleID
    delete from CARD_CARD_PROPERTY_LINKS where CARD_CODE in (select CARD_CODE from CARD_CARDS where PEOPLE_ID = @PeopleID)
    delete from CARD_TRANSACTION_OPERATIONS where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where ACCOUNT_ID in (select PEOPLE_ACCOUNT_ID from CARD_PEOPLE_ACCOUNTS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTION_NOTES where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where ACCOUNT_ID in (select PEOPLE_ACCOUNT_ID from CARD_PEOPLE_ACCOUNTS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTIONS where ACCOUNT_ID in (select PEOPLE_ACCOUNT_ID from CARD_PEOPLE_ACCOUNTS where PEOPLE_ID = @PeopleID)
    delete from CARD_TRANSACTION_OPERATIONS where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where ACCOUNT_ID in (select COUPON_ID from CARD_COUPONS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTION_NOTES where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where ACCOUNT_ID in (select COUPON_ID from CARD_COUPONS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTIONS where ACCOUNT_ID in (select COUPON_ID from CARD_COUPONS where PEOPLE_ID = @PeopleID)
    delete from CARD_TRANSACTION_OPERATIONS where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where CARD_CODE in (select CARD_CODE from CARD_CARDS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTION_NOTES where TRANSACTION_ID in (select TRANSACTION_ID from CARD_TRANSACTIONS where CARD_CODE in (select CARD_CODE from CARD_CARDS where PEOPLE_ID = @PeopleID))
    delete from CARD_TRANSACTIONS where CARD_CODE in (select CARD_CODE from CARD_CARDS where PEOPLE_ID = @PeopleID)
    delete from CARD_CONTRACT_PEOPLES where (PEOPLE_ID = @PeopleID) or (LINK_ID = @PeopleID) or (CONTRACT_ID in (select CONTRACT_ID from CARD_CONTRACTS where PEOPLE_ID = @PeopleID))
    delete from CARD_CONTRACT_DOCUMENTS where (PEOPLE_ID = @PeopleID) or (DOCUMENT_ID in (select DOCUMENT_ID from CARD_DOCUMENTS where PEOPLE_ID = @PeopleID)) or (CONTRACT_ID in (select CONTRACT_ID from CARD_CONTRACTS where PEOPLE_ID = @PeopleID))
    delete from CARD_CONTRACT_CONTACTS where (PEOPLE_ID = @PeopleID) or (CONTACT_ID in (select CONTACT_ID from CARD_CONTACTS where PEOPLE_ID = @PeopleID)) or (CONTRACT_ID in (select CONTRACT_ID from CARD_CONTRACTS where PEOPLE_ID = @PeopleID))
    delete from CARD_CONTRACT_ADDRESSES where (PEOPLE_ID = @PeopleID) or (ADDRESS_ID in (select ADDRESS_ID from CARD_ADDRESSES where PEOPLE_ID = @PeopleID)) or (CONTRACT_ID in (select CONTRACT_ID from CARD_CONTRACTS where PEOPLE_ID = @PeopleID))
    delete from CARD_CONTRACT_CARDS where (PEOPLE_ID = @PeopleID) or (CARD_CODE in (select CARD_CODE from CARD_CARDS where PEOPLE_ID = @PeopleID)) or (CONTRACT_ID in (select CONTRACT_ID from CARD_CONTRACTS where PEOPLE_ID = @PeopleID))
    delete from CARD_CONTRACTS where PEOPLE_ID = @PeopleID
    delete from CARD_DOCUMENTS where PEOPLE_ID = @PeopleID
    delete from CARD_RELATIVES where PEOPLE_ID = @PeopleID
    delete from CARD_ADDRESSES where PEOPLE_ID = @PeopleID
    delete from MSG_SENTS where CONTACT_ID in (select CONTACT_ID from CARD_CONTACTS where PEOPLE_ID = @PeopleID)
    delete from MSG_OUTGOINGS where PEOPLE_ID = @PeopleID
    delete from CARD_CONTACTS where PEOPLE_ID = @PeopleID
    delete from CARD_COUPON_CLIENTS where COUPON_ID in (select COUPON_ID from CARD_COUPONS where PEOPLE_ID = @PeopleID)
    delete from CARD_COUPONS where PEOPLE_ID = @PeopleID
    update CARD_CARDS set OWNER_ID = null where OWNER_ID = @PeopleID
    delete from CARD_CARDS where PEOPLE_ID = @PeopleID
    delete from CARD_PEOPLE_ACCOUNTS where PEOPLE_ID = @PeopleID
    delete from CARD_PEOPLE_PROPERTY_LINKS where PEOPLE_ID = @PeopleID
    delete from CARD_PEOPLES where PEOPLE_ID = @PeopleID

    set @i -= 1
    print 'One ('+cast(@PeopleID as nvarchar(20))+') deleted'

    fetch next from plist into @PeopleID
end

close plist
deallocate plist

delete from CARD_PEOPLE_ACCOUNT_OPERATIONS
delete from CARD_PEOPLE_ACCOUNTS_CARD
delete from CARD_CARD_CLIENTS
delete from LOG_USERS_ACTIVITY
delete from LOG_OPERATION_DETAILS
delete from LOG_OPERATIONS
delete from CARD_IMPORT