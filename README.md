﻿# SigurToCRM

Утилита для интеграции СКУД "СИГУР" с R-Keeper.CRM7

#### Установка
- Скачать|собрать sigur2crm.exe
- Запустить и настроить связь с БД MSSQL CRM и СИГУР
- Выполнять синхронизацию и экспорт в файл вручную или по расписанию с
  использованием параметров командной строки
- Используя выгруженный файл cards.csv настроить в R-Keeper MCR-алгоритм, задав
  скрипт обработчика:
```
function MCR1000442(DeviceSignal: Integer; DeviceIdent: Integer; var Parameter: String): Boolean;
var
  i, j, t1, t2, S, res1: Integer;
  codes : TStringlist;
begin
  Result := false;

  if pos(',',Parameter)>0 then
  begin
    codes := TStringList.Create;
    codes.LoadFromfile('d:\UCS\RK7\cash\cards.csv');
    t1 := codes.IndexOfName(Parameter);
    if (t1<0) then
       Result := False
    else
    begin
      Result := true;
      Parameter := copy(codes.strings[t1],
                      pos('=',codes.strings[t1])+1,
                      length(codes.strings[t1])-pos('=',codes.strings[t1]));
    end;
  end;
end;
```

#### Параметры запуска
- --do_create_csv - выполняет выгрузку данных из БД CRM в файл соответствий
- --do_sync - выполняет синхронизацию БД СИГУР и R-Keeper.CRM
- --give_money - Автопополнение до 1М при балансе ниже 1К
- --do_close - завершает работу приложения, когда все задания выполнены

