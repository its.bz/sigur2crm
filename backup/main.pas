unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mssqlconn, sqldb, DB, Forms, Controls, Graphics,
  Dialogs, StdCtrls, IniPropStorage, DBGrids, ComCtrls, LCLType,
  ExtCtrls, DBCtrls, vsVisualSynapse, SynHighlighterXML, fphttpclient;

type

  { TMainForm }

  TMainForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    crm_find3: TSQLQuery;
    deleteUsers: TCheckBox;
    crm_card_group_id: TDBLookupComboBox;
    crm_connection1: TMSSQLConnection;
    crm_delete1: TSQLQuery;
    crm_ds1: TDataSource;
    crm_ds2: TDataSource;
    crm_find1: TSQLQuery;
    crm_find2: TSQLQuery;
    crm_list1: TSQLQuery;
    crm_list2: TSQLQuery;
    crm_trans1: TSQLTransaction;
    crm_update: TSQLQuery;
    crm_update1: TSQLQuery;
    crm_delete: TSQLQuery;
    crm_update2: TSQLQuery;
    crm_update3: TSQLQuery;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    crm_http: TEdit;
    crm_account_type_id: TDBLookupComboBox;
    GroupBox3: TGroupBox;
    crm_find: TSQLQuery;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    LogsMemo: TMemo;
    ProgressBar1: TProgressBar;
    StatusLabel: TLabel;
    makeCsvButton: TButton;
    SaveButton: TButton;
    SaveButton1: TButton;
    crm_ds: TDataSource;
    Memo1: TMemo;
    sigur_list: TSQLQuery;
    sigur_trans: TSQLTransaction;
    sigur_ds: TDataSource;
    sigur_connection: TMSSQLConnection;
    crm_list: TSQLQuery;
    crm_trans: TSQLTransaction;
    SyncButton: TButton;
    testCRMButton: TButton;
    testSigurButton: TButton;
    crm_db: TEdit;
    sigur_db: TEdit;
    crm_host: TEdit;
    sigur_host: TEdit;
    crm_login: TEdit;
    sigur_login: TEdit;
    crm_password: TEdit;
    sigur_password: TEdit;
    GroupBox1: TGroupBox;
    crm_connection: TMSSQLConnection;
    GroupBox2: TGroupBox;
    settings: TIniPropStorage;
    HTTP1: TvsVisualHTTP;
    Timer1: TTimer;
    procedure Button6Click(Sender: TObject);
    procedure crm_account_type_idChange(Sender: TObject);
    procedure crm_account_type_idCloseUp(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure crm_card_group_idChange(Sender: TObject);
    procedure crm_listAfterOpen(DataSet: TDataSet);
    procedure deleteUsersClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure GroupBox3Click(Sender: TObject);
    procedure makeCsvButtonClick(Sender: TObject);
    procedure sigur_listAfterOpen(DataSet: TDataSet);
    procedure SyncButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure testCRMButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure testSigurButtonClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    ACCOUNT_TYPE_ID: string;
    CARD_GROUP_ID: string;
    procedure Log(s: string; toLabel: boolean = True; toMemo: boolean = True;
      toFile: boolean = True);
    function CrmFindByCarrierData(carrier_data: string): string;
  public

  end;

var
  MainForm: TMainForm;
  logFile: TextFile;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.Log(s: string; toLabel: boolean = True;
  toMemo: boolean = True; toFile: boolean = True);
var
  timestamp: string;
begin
  timestamp := '[' + DateToStr(Now) + ' ' + TimeToStr(Now) + '] ';
  if (toFile) then
    WriteLn(logFile, timestamp + s);

  if (toLabel) then
  begin
    StatusLabel.Caption := s;
    StatusLabel.Refresh;
  end;

  if (toMemo) then
    LogsMemo.Lines.Insert(0, timestamp + s);
end;

procedure TMainForm.testCRMButtonClick(Sender: TObject);
var
  ReadURL: TStringStream;
  xml: string;
  xmlFile: TStringList;
begin
  Log('Тест соединения CRM');

  // Connection for selects
  crm_connection.Connected := False;
  crm_connection.HostName := crm_host.Text;
  crm_connection.UserName := crm_login.Text;
  crm_connection.Password := crm_password.Text;
  crm_connection.DatabaseName := crm_db.Text;

  // Connection for updates
  crm_connection1.Connected := False;
  crm_connection1.HostName := crm_host.Text;
  crm_connection1.UserName := crm_login.Text;
  crm_connection1.Password := crm_password.Text;
  crm_connection1.DatabaseName := crm_db.Text;
  try
    crm_connection.Connected := True;
    crm_list.Active := True;
    crm_list1.Active := True;
    crm_account_type_id.KeyValue := ACCOUNT_TYPE_ID;
    crm_list2.Active := True;
    crm_card_group_id.KeyValue := CARD_GROUP_ID;
    Log('Связь с БД установлена успешно!');
    Log('CRM: Загружено ' + IntToStr(crm_list.RecordCount) + ' записей');
    testCRMButton.Caption:= 'Test OK';
  except
    on e: Exception do
    begin
      Log('Связь с БД не установлена!');
      testCRMButton.Caption:= 'Test FAIL';
      exit;
    end;
  end;

  xmlFile := TStringList.Create;
  xmlFile.LoadFromFile('xml\get_version.xml');
  xml := xmlFile.Text;
  ReadURL := TStringStream.Create('');
  with TFPHTTPClient.Create(nil) do
  begin
    try
      RequestBody := TStringStream.Create(xml);
      Post(crm_http.Text, ReadURL);
      Log('Связь с HTTP установлена успешно!');
    except
      on e: Exception do
      begin
        Log('Связь с HTTP не установлена!');
        testCRMButton.Color := clRed;
      end;
    end;
    ReadURL.Free;
    RequestBody.Free;
    Free;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  AssignFile(logFile, ExtractFileName(Application.ExeName) + '.log');
  Rewrite(logfile);

  Log('Запуск программы');
  Log('Чтение настроек');
  settings.IniSection := 'CRM';
  crm_host.Text := settings.ReadString('host', 'host');
  crm_login.Text := settings.ReadString('login', 'login');
  crm_password.Text := settings.ReadString('password', 'password');
  crm_db.Text := settings.ReadString('db', 'db');
  crm_http.Text := settings.ReadString('http', 'http://127.0.0.1:9192');
  ACCOUNT_TYPE_ID := settings.ReadString('account_type_id', '4');
  CARD_GROUP_ID := settings.ReadString('card_group_id', '4');
  deleteUsers.Checked := settings.ReadBoolean('deleteUsers', False);


  settings.IniSection := 'SIGUR';
  sigur_host.Text := settings.ReadString('host', 'host');
  sigur_login.Text := settings.ReadString('login', 'login');
  sigur_password.Text := settings.ReadString('password', 'password');
  sigur_db.Text := settings.ReadString('db', 'db');
end;

function TMainForm.CrmFindByCarrierData(carrier_data: string): string;
begin
  Result := '';
  crm_find1.Active := False;
  crm_find1.ParamByName('CARRIER_DATA').AsString := carrier_data;
  crm_find1.Active := True;
  if (crm_find1.RecordCount > 0) then
    Result := crm_find1.FieldByName('CARD_CODE').AsString;
  exit;
end;

procedure TMainForm.SyncButtonClick(Sender: TObject);
var
  ReadURL: TStringStream;
  xmlFile: TStringList;
  xml: string;
  ID, Name_, KEY_W26, Position_, CARD_CODE, code, PEOPLE_ID: string;
  i, was_updated, was_created, was_deleted: integer;
begin
  was_updated := 0;
  was_created := 0;
  was_deleted := 0;

  if (not crm_connection.Connected) then
    testCRMButton.Click;
  if (not sigur_connection.Connected) then
    testSIGURButton.Click;
  Log('Синхронизация..');
  i := 0;
  xmlFile := TStringList.Create;
  sigur_list.First();
  if (sigur_list.RecordCount = 0) then
  begin
    ShowMessage('Данные СИГУР не найдены');
    exit;
  end;

  // Добавление новых и обновление имеющихся людей в CRM
  ProgressBar1.Max := sigur_list.RecordCount;
  repeat
    Inc(i);
    CARD_CODE := IntToStr(i);
    ProgressBar1.Position := i;
    ID := sigur_list.FieldByName('ID').AsString;
    Name_ := sigur_list.FieldByName('Name').AsString;
    Position_ := sigur_list.FieldByName('Position').AsString;
    KEY_W26 := sigur_list.FieldByName('KEY_W26').AsString;
    sigur_list.Next;

    // Карта, у которой код брелка совпадает, но ID не совпадает
    crm_find3.Active := False;
    crm_find3.ParamByName('CD_LIKE').AsString := '%::' + KEY_W26;
    crm_find3.ParamByName('CD').AsString := ID + '::' + KEY_W26;
    crm_find3.Active := True;
    if (crm_find3.RecordCount = 1) then
      if (deleteUsers.Checked) then
      begin
        // Карты, у которых код брелка совпадает, но ID не совпадает удаляются из
        // базы CRM вместе со всей историей.
        // Это происходит, когда пользователя удаляют из БД СИГУР и создают нового
        // с тем же брелком.
        // В CRM удаляем людей.
        // А затем создаём нового человека с этим кодом и новым ID в CARRIER_DATA.
        was_deleted := was_deleted + 1;
        Log('DELETED: ' + crm_find3.FieldByName('PEOPLE_ID').AsString);
        PEOPLE_ID := crm_find3.FieldByName('PEOPLE_ID').AsString;
        crm_delete1.ParamByName('PEOPLE_ID').AsString := PEOPLE_ID;
        crm_delete1.ExecSQL;
      end
      else
      begin
        // префикс "-" в CARRIER_DATA.
        // Это происходит, когда пользователя удаляют из БД СИГУР и создают нового
        // с тем же брелком.
        // В CRM удалённых людей оставляем, но CARRIER_DATA добавляем префикс "-".
        // А затем создаём нового человека с этим кодом и новым ID в CARRIER_DATA.
        crm_update1.ParamByName('CD_LIKE').AsString := '%::' + KEY_W26;
        crm_update1.ParamByName('CD').AsString := ID + '::' + KEY_W26;
        crm_update1.ExecSQL;
      end;

    code := CrmFindByCarrierData(ID + '::' + KEY_W26);
    if (code <> '') then
    begin // UPDATE
      if (crm_find1.FieldByName('CARRIER_DATA').AsString <> ID + '::' + KEY_W26) then
      begin
        was_updated := was_updated + 1;
        Log('UPDATED: ' + crm_list.FieldByName('CARD_CODE').AsString + ': ' + Name_);
        crm_update.ParamByName('CARD_CODE').AsString := code;
        crm_update.ParamByName('CARRIER_DATA').AsString := ID + '::' + KEY_W26;
        crm_update.ExecSQL;
      end;
    end
    else
    begin // INSERT
      was_created := was_created + 1;
      crm_find2.Active := True;
      CARD_CODE := crm_find2.FieldByName('n').AsString;
      if (CARD_CODE = '') then
        CARD_CODE := '1';
      crm_find2.Active := False;
      Log('CREATED: ' + CARD_CODE + ': ' + Name_);

      xmlFile.LoadFromFile('xml\add_holders.xml');
      xml := xmlFile.Text;
      xml := xml.Replace('#Full_Name#', Name_, [rfReplaceAll]);
      xml := xml.Replace('#Remarks#', '[' + ID + '] ' + Position_, [rfReplaceAll]);
      xml := xml.Replace('#Carrier_Data#', ID + '::' + KEY_W26, [rfReplaceAll]);
      xml := xml.Replace('#Card_Code#', CARD_CODE, [rfReplaceAll]);
      xml := xml.Replace('#Source#', 'СИГУР via ITS.bz', [rfReplaceAll]);
      xml := xml.Replace('#External_Code#', ID, [rfReplaceAll]);
      xml := xml.Replace('#Card_Group_ID#', CARD_GROUP_ID, [rfReplaceAll]);


      with TFPHTTPClient.Create(nil) do
        try
          ReadURL := TStringStream.Create('');
          RequestBody := TStringStream.Create(xml);
          Post(crm_http.Text, ReadURL);
          Memo1.Text := ReadURL.DataString;
          ReadURL.Free;
          RequestBody.Free;
          Free;
        except
          on e: Exception do
          begin
            Log(e.ToString);
            ReadURL.Free;
            RequestBody.Free;
            Free;
          end;
        end;
    end;
    Application.ProcessMessages;
    ProgressBar1.Repaint;
  until i >= sigur_list.RecordCount;
  crm_list.Refresh;
  Log('Обновлено: ' + IntToStr(was_updated) + ', Создано: ' +
    IntToStr(was_created) + ', Удалено: ' + IntToStr(was_deleted));
end;

procedure TMainForm.makeCsvButtonClick(Sender: TObject);
var
  list: TStringList;
  i: integer;
  CARD_CODE, Carrier_Data: string;
begin
  Log('Экспорт в файл..');
  list := TStringList.Create;

  i := 0;
  crm_list.First();
  if (crm_list.RecordCount = 0) then
  begin
    ShowMessage('Данные CRM не найдены');
    exit;
  end;

  ProgressBar1.Max := crm_list.RecordCount;

  repeat
    Inc(i);
    ProgressBar1.Position := i;
    CARD_CODE := crm_list.FieldByName('CARD_CODE').AsString;
    Carrier_Data := crm_list.FieldByName('CARRIER_DATA').AsString;
    list.Add(RightStr(Carrier_Data, Length(Carrier_Data) - Pos('::', Carrier_Data) - 1) +
      '=' + CARD_CODE);
    crm_list.Next;
    Application.ProcessMessages;
  until i >= crm_list.RecordCount;
  list.SaveToFile('cards.csv');
  list.Free;
  Log('Экспорт в файл завершён');
end;

procedure TMainForm.sigur_listAfterOpen(DataSet: TDataSet);
var
  i: integer;
begin
  for i := 0 to DBGrid2.Columns.Count - 1 do
    DBGrid2.Columns.Items[i].Width := DBGrid2.Width div DBGrid2.Columns.Count - 12;
end;

procedure TMainForm.crm_listAfterOpen(DataSet: TDataSet);
var
  i: integer;
begin
  for i := 0 to DBGrid1.Columns.Count - 1 do
    DBGrid1.Columns.Items[i].Width := DBGrid1.Width div DBGrid1.Columns.Count - 12;
end;

procedure TMainForm.deleteUsersClick(Sender: TObject);
begin
  settings.IniSection := 'CRM';
  settings.WriteBoolean('deleteUsers', deleteUsers.Checked);
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.crm_account_type_idChange(Sender: TObject);
begin
  ACCOUNT_TYPE_ID := crm_account_type_id.KeyValue;
end;

procedure TMainForm.Button6Click(Sender: TObject);
begin
  crm_update3.ExecSQL;
  crm_list.Refresh;
end;

procedure TMainForm.crm_account_type_idCloseUp(Sender: TObject);
begin

end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  if (Application.MessageBox('Удалить со всей историей?',
    'Удаление', MB_ICONQUESTION + MB_YESNO) = idYes) then
  begin
    crm_delete1.ParamByName('PEOPLE_ID').AsString :=
      crm_list.FieldByName('PEOPLE_ID').AsString;
    crm_delete1.ExecSQL;
    crm_list.Refresh;
  end;
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
  if (Application.MessageBox('Удалить всё?!!', 'Удаление',
    MB_ICONQUESTION + MB_YESNO) = idYes) then
    if (Application.MessageBox('Прям вот совсем всё-всё?!!',
      'Удаление', MB_ICONQUESTION + MB_YESNO) = idYes) then
    begin
      crm_delete.ParamByName('FULL_NAME').AsString := '%';
      crm_delete.ExecSQL;
      crm_list.Refresh;
    end;
end;

procedure TMainForm.Button4Click(Sender: TObject);
begin
  crm_update2.ParamByName('PEOPLE_ID').AsString := '%';
  crm_update2.ExecSQL;
  crm_list.Refresh;
end;

procedure TMainForm.Button5Click(Sender: TObject);
begin
  crm_update2.ParamByName('PEOPLE_ID').AsString :=
    crm_list.FieldByName('PEOPLE_ID').AsString;
  crm_update2.ExecSQL;
  crm_list.Refresh;
end;

procedure TMainForm.crm_card_group_idChange(Sender: TObject);
begin
  CARD_GROUP_ID := crm_card_group_id.KeyValue;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  Log('Завершение программы');
  crm_connection.Close();
  crm_connection1.Close();
  sigur_connection.Close();
  CloseFile(logFile);
end;

procedure TMainForm.GroupBox3Click(Sender: TObject);
begin

end;

procedure TMainForm.SaveButtonClick(Sender: TObject);
begin
  settings.IniSection := 'CRM';
  settings.WriteString('host', crm_host.Text);
  settings.WriteString('login', crm_login.Text);
  settings.WriteString('password', crm_password.Text);
  settings.WriteString('db', crm_db.Text);
  settings.WriteString('http', crm_http.Text);
  settings.WriteString('account_type_id', crm_account_type_id.KeyValue);
  settings.WriteString('card_group_id', crm_card_group_id.KeyValue);


  settings.IniSection := 'SIGUR';
  settings.WriteString('host', sigur_host.Text);
  settings.WriteString('login', sigur_login.Text);
  settings.WriteString('password', sigur_password.Text);
  settings.WriteString('db', sigur_db.Text);
end;

procedure TMainForm.testSigurButtonClick(Sender: TObject);
begin
  sigur_connection.Connected := False;
  sigur_connection.HostName := sigur_host.Text;
  sigur_connection.UserName := sigur_login.Text;
  sigur_connection.Password := sigur_password.Text;
  sigur_connection.DatabaseName := sigur_db.Text;
  try
    sigur_connection.Connected := True;
    sigur_list.Active := True;
    Log('Связь установлена успешно!');
    Log('СИГУР: Загружено ' + IntToStr(sigur_list.RecordCount) +
      ' записей');
    testSigurButton.Caption := 'Test OK';
  except
    on e: Exception do
    begin
      Log('Связь с БД не установлена!');
      testSigurButton.Caption := 'Test FAIL';
    end;
  end;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
  i: integer;
  do_close, do_create_csv, do_sync, give_money, delete_from_crm: boolean;
begin
  Timer1.Enabled := False;
  if (Paramcount > 0) then
  begin
    do_close := False;
    do_sync := False;
    do_create_csv := False;
    give_money := False;
    delete_from_crm := False;
    for i := 1 to Paramcount do
    begin
      case ParamStr(i) of
        '--do_sync': do_sync := True;
        '--do_create_csv': do_create_csv := True;
        '--do_close': do_close := True;
        '--give_money': give_money := True;
        '--delete_from_crm': delete_from_crm := True;
        else
          do_close := False;
      end;
    end;
    if (do_sync) or (do_create_csv) or (give_money) then
    begin
      testCRMButton.Click;
      testSigurButton.Click;
      deleteUsers.Checked := delete_from_crm;
    end;
    if (do_sync) then
      SyncButton.Click;
    if (do_create_csv) then
      makeCsvButton.Click;
    if (give_money) then
      Button6.Click;
    if (do_close) then
      Application.Terminate;
  end;
end;

end.
